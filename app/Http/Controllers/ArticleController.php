<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Comment;
use App\Category;
use Illuminate\Support\Str;
class ArticleController extends Controller
{
    //
    public function allArticle()
    {

        $articles = Article::paginate(4);
        return view('articles.allArticle',compact('articles'));
    }
    public function showArticle($id)
    {
        # code...
        $article = Article::findOrFail($id);
        $comments = $article->comments()->paginate(4);
        return view('articles.showArticle', 
                    [
                        'article'=>$article,
                        'comments' =>$comments
                    ]
                    );
    }
    public function createArticle()
    {
        return view('articles.createArticle');
    }
    public function postArticle(Request $request)
    {
        Article::create([
            'title'=>$request->title,
            'content' => $request->content
        ]);
        return redirect(route('lastArticle'));
    }
    public function lastArticle()
    {
        $id = Article::max('id');
        return $this->showArticle($id);   
    }
    public function editArticle($id)
    {
        $article = Article::findOrFail($id);
        return view('articles.editArticle', compact('article'));   
    }
    public function updateArticle(Request $request, $id)
    {
        $article = Article::findOrFail($id);
        $article->title = $request->title;
        $article->content = $request->content;
        $article->save();
        return redirect(route('showArticle', $id));   
    }
    public function deleteArticle(Request $request,$id)
    {
        Article::findOrFail($id)->delete();
        return redirect(route('homePage'));   
    }
    public function searchArticle(Request $request)
    {
        $articles = Article::where('title','like','%'.$request->key.'%')
                                ->orWhere('content','like','%'.$request->key.'%')
                                ->paginate(4);
        return view('articles.allArticle',compact('articles'));
    }
    public function addComment(Request $request)
    {
        Comment::create([
                'article_id'=> $request->article_id,
                'content'=>$request->content
        ]); 
        return redirect(route('showArticle',$request->article_id));
    }
    public function categoryArticles($id)
    {
        $articles = Category::findOrFail($id)->category_articles()->paginate(4);
        return view('articles.allArticle',['articles'=>$articles]);
    }
}
