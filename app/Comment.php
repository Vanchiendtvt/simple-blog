<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $fillable = ['article_id','content'];
    public function article()
    {
        return $this->belogTo('App\Comment');
    }
}
