<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    public $fillable = ['title', 'content'];
    public function comments(){
        return $this->hasMany('App\Comment');
    }
    public function categories()
    {
        return $this->belongsToMany('App\Category','category_articles');
    }
}
