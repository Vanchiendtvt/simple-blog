<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function category_articles(Type $var = null)
    {
        return $this->belongsToMany('App\Article','category_articles');
    }
}
