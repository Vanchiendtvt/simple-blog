@extends ('layouts.master')
@section('head.title')
Tat ca bai viet tren blog   
@stop 
@section('body.content')
<div id="modest" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-3 offset-9" >
                <form action="{{route('searchArticle')}}" method = "get">
                    <input type="text" placeholder="Tim kiem" name ="key">
                    <!-- <input type = "submit" value = "Tìm kiếm"> -->
                </form>
            </div>
        </div>
        <div class="row"><h2 class="col-auto">what is my blog.</h2></div>
        <div class="row">
            <div class="col-7">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nisi metus,
                    tristique ndolor non, ornare sagittis dolor. Nulla vestibulu lacus sed
                    molestie gravida. Crferm entum  quismagna congue, vel sodales arcu
                    vestibulum. Nunc lobortis dui magna, quis lacusullamcorper at.</p> 
                <p>Phasellus sollicitudin ante eros ornare, <span>sit amet luctus
                    lorem semper</span>. Suspendisse posuere, quamdictum consectetur, 
                    augue metus pharetra tellus, eu feugiatloreg egetnisi. Cras 
                    ornare bibendum ante, ut bibendum odio convallis eget. vel sodales
                    arcu vestibulum</p>
                <div id="socials">
                    <a href="#"><img src="{{ url('/images/facebook.png') }}" alt=""></a>
                    <a href="#"><img src="{{ url('/images/twitter.png') }}" alt=""></a>
                    <a href="#"><img src="{{ url('/images/googleplus.png') }}" alt=""></a>
                    <a href="#"><img src="{{ url('/images/linkedin.png') }}" alt=""></a>
                    <a href="#"><img src="{{ url('/images/behance.png') }}" alt=""></a>
                </div>
            </div>
            <div class="col-5 listArticle">
                    <ul>
                        @for ($i = 0; $i < $articles->count(); $i++)
                        <li>
                            <span>{{($articles->currentPage()-1) *4 +$i+1}}</span>
                            <h3><a href="{{route('showArticle',$articles[$i]['id'])}}">{{$articles[$i]['title']}}</a></h3>
                            <p>{{ $articles[$i]['content']}}</p>
                            <a href="{{route('editArticle',$articles[$i]['id'])}}">Sửa</a>
                            <a href="{{route('deleteArticle',$articles[$i]['id'])}}">Xóa</a>
                            
                        </li>
                        @endfor
                    </ul>
                    <div> {{ $articles->render() }} </div>
                    
                </div>
        </div>
    </div>
</div>
@stop