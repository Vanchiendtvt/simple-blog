@extends ('layouts.master')

@section('head.title')
Tao bai viet moi
@stop 

@section('body.content')
<div class="container" id='createArticle'>
    <h2>Tạo bài viết mới: </h2>
    <form action="{{ route('postArticle') }}" method="post">
        @csrf
        <table>
            <tr>
                <th>Tiêu đề:  </th>
                <td><input type="text" name="title" required></td>
            </tr>
            <tr>
                <th>Nội dung bài viết:  </th>
                <td><textarea name="content" id="" cols="60" rows="20" required></textarea></td>
            </tr>
        </table>
        <input type="submit" value="Đăng bài viết">
    </form>
</div>

@stop