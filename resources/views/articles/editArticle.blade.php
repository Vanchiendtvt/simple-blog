@extends ('layouts.master')

@section('head.title')
Sửa bài viết
@stop 

@section('body.content')
<div class="container" id='createArticle'>
    <h2>Sửa bài viết: </h2>
    <form action="{{ route('updateArticle',$article->id) }}" method="post">
        @csrf
        <table>
            <tr>
                <th>Tiêu đề:  </th>
                <td><input type="text" name="title" value="{{$article->title}}"></td>
            </tr>
            <tr>
                <th>Nội dung bài viết:  </th>
                <td><textarea name="content" id="" cols="60" rows="20" >{{$article->content}}</textarea></td>
            </tr>
        </table>
        <input type="submit" value="Hoàn tất">
    </form>
</div>

@stop