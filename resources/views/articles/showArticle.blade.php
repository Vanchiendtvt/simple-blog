@extends('layouts.master')
@section('head.title')
{{ $article->title}}
@stop 

@section('body.content')
<div id="showArticle" class="container-fluid">
    <div class="container">
        <div class="row" id="article">
            <div class="col-10" >
                <h2>{{ $article->title }}</h2>
                <p>{{ $article->content }}</p>
            </div>
            <div class="col-10" id="categories">
                <span>Categories: </span>
                @foreach ($article->categories as $category)
                    <a href="{{route('categoryArticles',$category->id)}}">{{$category->name}},</a>
                @endforeach
            </div>
        </div>
        <div class ="row" id="comment">
            <div class="col-7">
                <h3>Bình luận: </h3>
                <ul>
                    @if ($comments->total()==0)
                        <li>Hãy là người đầu tiên bình luận cho bài viết này</li>
                    @else
                        @foreach($comments as $comment)
                            <li>{{$comment->content}}</li>
                        @endforeach
                    @endif
                </ul>
                <div>{{$comments->render()}}</div>
                <form action="{{route('addComment')}}" method="post">
                    @csrf
                    <input type ="hidden"  name="article_id" value="{{$article->id}}">
                    <textarea name="content" cols="60" rows="3" placeholder="Bình luận" required></textarea><br>
                    <input type="submit" value="Gửi">
                </form>
            </div>
        </div>
    </div>
</div>

@stop