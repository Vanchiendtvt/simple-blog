<div id= "nav" class="container-fluid">
    <div class="container">
        <div class="row">
            <a href="{{ route('homePage') }}" class="col-md-1"><img src="{{ url('/images/logo-Modest.png')}}" alt="" ></a>
            <ul class="col-auto">
                <li><a href="{{ route('homePage') }}">Home</a></li>
                <li><a href="{{ route('createArticle') }}">Create Article</a></li>
                <li><a href="#">Work</a></li>
                <li><a href="#">Features</a></li>
                <li><a href="#">Contacts</a></li>
            </ul>
        </div>
    </div>
</div>