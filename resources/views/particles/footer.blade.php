<div id="footer" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-6"><p>© Copyright 2014 by PSD Booster. All Rights Reserved.</p></div>
                <ul class="col-3 offset-3">
                    <li><a href="#"><img src="{{ url('/images/twitter.png')}}" alt=""></a></li>
                    <li><a href="#"><img src="{{ url('/images/facebook.png')}}" alt=""></a></li>
                    <li><a href="#"><img src="{{ url('/images/skype.png')}}" alt=""></a></li>
                    <li><a href="#"><img src="{{ url('/images/behance.png')}}" alt=""></a></li>
                    <li><a href="#"><img src="{{ url('/images/linkedin.png')}}" alt=""></a></li>
                </ul>
            </div>
        </div>
</div>