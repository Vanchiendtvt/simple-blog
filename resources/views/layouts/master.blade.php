<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('/style.css')}}">
    <title>@yield('head.title')</title>
</head>
<body>
    @include('particles/navbar')
    @yield('body.content')
    @include('particles/footer')
</body>
</html>