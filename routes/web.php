<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('homePage');
Route::get('/test', 'PagesController@test')->name('test');
Route::get('/article/all', 'ArticleController@allArticle')->name('allArticle');
Route::get('/article/create', 'ArticleController@createArticle') ->name('createArticle');
Route::get('/article/edit/{id}', 'ArticleController@editArticle') ->name('editArticle');
Route::post('/article/update/{id}', 'ArticleController@updateArticle') ->name('updateArticle');
Route::get('/article/delete/{id}', 'ArticleController@deleteArticle') ->name('deleteArticle');
Route::get('/article/last', 'ArticleController@lastArticle') ->name('lastArticle');
Route::get('/article/search', 'ArticleController@searchArticle') ->name('searchArticle');
Route::post('/article/post', 'ArticleController@postArticle') ->name('postArticle');
Route::post('/article/addComment', 'ArticleController@addComment') ->name('addComment');
Route::get('/article/categoryArticles/{id}', 'ArticleController@categoryArticles')->name('categoryArticles');
Route::get('/article/{id}', 'ArticleController@showArticle')->name('showArticle');

