<?php

use Illuminate\Database\Seeder;
use App\Category_article;

class CategoryArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=0; $i < 200; $i++) { 
            Category_article::create(
                [
                    'category_id' => $faker->numberBetween(1, 9),
                    'article_id' => $faker->numberBetween(1, 100)
                ]
            );
        }
    }
}
