<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i=0; $i <100;$i++){
            Comment::create([
                'article_id'=>$faker->numberBetween(1,39),
                'content'=>$faker->sentence()
            ]);
        }
    }
}
